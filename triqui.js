class Triqui {

    constructor() {

        this.matriz = [
            ["c1", "c2", "c3"],
            ["c4", "c5", "c6"],
            ["c7", "c8", "c9"]
        ];
        this.dibujarTablero();
        this.definirJugador();
        


    }

    dibujarTablero() {

        //Crear un objeto que referencie a la división tablero
        let miTablero = document.getElementById("tablero");

        for (let i = 0; i < 9; i++) {
            miTablero.innerHTML = miTablero.innerHTML + "<input type='text' id='casilla" + (i + 1) + "' class='casilla' onclick='miTriqui.realizarJugada()'>";

            if ((i + 1) % 3 == 0) {
                miTablero.innerHTML = miTablero.innerHTML + "<br>";
            }

        }
    }

    definirJugador() {
        let n;

        let miTurno = document.getElementById("turno");
        n = Math.round(Math.random() + 1);

        if (n === 1) {
            this.turno = "X";
        } else {
            this.turno = "O";
        }
        miTurno.innerHTML = "Es el turno de: " + this.turno;
    }

    realizarJugada(){
        let miElemento=event.target;
        if(!(miElemento.value==="X" || miElemento.value==="O")){
            miElemento.value=this.turno;
            this.modificarMatriz(miElemento.id);


            if(this.verificarTriqui()===true){
                document.getElementById("resultado").innerHTML="¡¡¡"+this.turno+" Haz ganado!";
    
            }else{
                this.cambiarTurno();
                if(this.verificarTriqui()===false){
                    document.getElementById("resultado").innerHTML="Ninguno ha ganado";
                }else{
                    this.cambiarTurno();
                }
            }
        }else{
            document.getElementById("error").innerHTML="La casilla esta llena";
        }
        
    }


    verificarTriqui() {

        let triqui = false;

        

        for (let fila = 0; fila < 3; fila++) {
            if (this.matriz[fila][0] === this.matriz[fila][1] && this.matriz[fila][0]=== this.matriz[fila][2]) {
                triqui = true;
                return triqui;
            }
        }

        for (let columna = 0; columna < 3; columna++) {
            if (this.matriz[0][columna] === this.matriz[1][columna] && this.matriz[0][columna]=== this.matriz[2][columna]) {
                triqui = true;
                return triqui;
            }
        }

        if (this.matriz[0][0] === this.matriz[1][1] && this.matriz[0][0]=== this.matriz[2][2]) {
            triqui = true;
            return triqui;
        }

        if (this.matriz[0][2] === this.matriz[1][1] && this.matriz[0][2] === this.matriz[2][0]) {
            triqui = true;
            return triqui;
        }


        return triqui;
    }

    


    modificarMatriz(id){

        for(let i=0; i<3; i++){  
            if(id === this.matriz[0][i]){
                    this.matriz[0][i] = this.turno;                
            }
        }
        for(let i=0; i<3; i++){  
            if(id === this.matriz[1][i]){
                    this.matriz[1][i] = this.turno;                
            }
        }
        for(let i=0; i<3; i++){  
            if(id === this.matriz[2][i]){
                    this.matriz[2][i] = this.turno;                
            }
        } 
    }




    cambiarTurno() {

        let miTurno = document.getElementById("turno");

        if (this.turno === "X") {
            this.turno = "O";
        } else {
            this.turno = "X";
        }
        miTurno.innerHTML = "Es el turno de: " + this.turno;

    }


    resset(){
        this.matriz = [
            ["c1", "c2", "c3"],
            ["c4", "c5", "c6"],
            ["c7", "c8", "c9"]
        ];
        for(let i=0;i<9;i++){
            let celda =document.getElementById("casilla"+(i+1));
            celda.value=" "; 
        }

        let ress =document.getElementById("resultado");
        ress.innerHTML=" ";

        let err = document.getElementById("error");
        err.innerHTML=" ";
    

    }



}